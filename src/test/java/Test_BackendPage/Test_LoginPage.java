package Test_BackendPage;


import Backend_Page.Login_Page;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import dataProviders.ConfigFileReader;
import dataProviders.JsonDataReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import resources.TextResources;
import utilities.BaseClass;
import utilities.WorkWithElements;

import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

import DataDriven.ReadJSON;

public class Test_LoginPage extends BaseClass {
    WebDriver driver;
    ConfigFileReader configFileReader= new ConfigFileReader();
    JsonDataReader jsonDataReader=new JsonDataReader();

    @Test(dataProvider = "dp")
    public void testValidLogin1(String data) throws InterruptedException {

        String users[] = data.split(",");
        Login_Page loginPage = new PageFactory().initElements(driver, Login_Page.class);
        Thread.sleep(5000);
        loginPage.getEmailTextField(users[0]);
        loginPage.getPasswordField(users[1]);
        loginPage.getLoginBtn();


        /*WorkWithElements.complete(loginPage.getEmailTextField(), TextResources.email);
        WorkWithElements.complete(loginPage.getPasswordField(), TextResources.password);
        WorkWithElements.clickElement(loginPage.getLoginBtn());
        assertTrue(driver.getPageSource().contains("View Website"));*/
    }

    @Test
    public void testValidLogin2() throws InterruptedException {


        Login_Page loginPage = new PageFactory().initElements(driver, Login_Page.class);
        loginPage.getEmailTextField(TextResources.email);
        loginPage.getPasswordField(TextResources.password);
        //loginPage.getLoginBtn().click();
        //WorkWithElements.clickElement(loginPage.getLoginBtn());

    }


}
