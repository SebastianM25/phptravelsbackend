package Backend_Page;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import resources.Customer;
import resources.TextResources;
import utilities.BaseClass;

public class Login_Page extends BaseClass {

    WebDriver driver;

    public Login_Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(name="email")
    private WebElement emailField;
    @FindBy(name="password")
    private WebElement passwordField;
    @FindBy(xpath = "//span[contains(text(),'Login')]")
    private WebElement loginBtn;

    public WebElement getEmailTextField(String email) {
        return emailField;
    }
    public WebElement getPasswordField(String password) {
        return passwordField;
    }
    public WebElement getLoginBtn() {
        return loginBtn;
    }




    /*public void fillEmailField(String user) {
        driver.findElement(emailField).sendKeys(user);
    }

    public void fillPasswordField(String user) {
        driver.findElement(passwordField).sendKeys(user);
    }*/

   /* public void clickOnLogin() {

        driver.findElement(loginBtn).click();
    }*/

    /*public void ValidLogin() {
        fillEmailField(TextResources.email);
        fillPasswordField(TextResources.password);
        clickOnLogin();
    }*/

    public void fill_PersonalDetails(Customer customer) {
        getEmailTextField(customer.email);
        getPasswordField(customer.password);

    }

}
