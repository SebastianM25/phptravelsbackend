package configuration;

import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;

public class ThreadWebDriver {

    private static ThreadLocal<WebDriverInstance> driverInstance = new ThreadLocal<>();

    public static void setDriverInstance(WebDriverInstance instance) throws MalformedURLException {
        driverInstance.set(instance);
    }

    public static WebDriverInstance getDriverInstance() {
        return driverInstance.get();
    }

    public static void removeDriverInstance() {
        driverInstance.remove();
    }
}
