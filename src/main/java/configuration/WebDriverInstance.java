package configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverInstance {
    protected WebDriver driver;

    private final String HUBIP = "https://www.phptravels.net/admin";
    private final URL hubUrl = new URL(HUBIP + "/wd/hub");

    public WebDriverInstance() throws MalformedURLException {
        createDriver();
    }

    private DesiredCapabilities setCapabilities() {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();

        return capabilities;
    }

    private void createDriver() {
        driver = new RemoteWebDriver(hubUrl, setCapabilities());
        driver.manage().window().maximize();
    }

    public WebDriver getDriver() {
        return this.driver;
    }
}
