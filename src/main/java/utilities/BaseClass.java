package utilities;

import configuration.WebDriverInstance;
import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
//import Configuration.WebDriverInstance;

public class BaseClass {


    public WebDriver driver;
    String url = "https://www.phptravels.net/admin";
    private DriversUtils driversPath = new DriversUtils();


    @Before
    public void setUpTestFireFox() {

        System.setProperty("webdriver.gecko.driver", driversPath.getDriverDirPath() + "geckodriver" + driversPath.getDriverExtension());
        driver = new FirefoxDriver();
        driver.get(url);
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
    }
    @Before
     /*public void setUpTestChrome() {
         System.setProperty("webdriver.chrome.driver", driversPath.getDriverDirPath() + "chromedriver" + driversPath.getDriverExtension());
         System.setProperty("webdriver.chrome.whitelistedIps", "");
         driver = new ChromeDriver();
         driver.get(url);
         if (SystemUtils.IS_OS_WINDOWS) {
             driver.manage().window().maximize();
         }
     }*/

    /*public void waitTime(int timeToWaitInMilliseconds) {
        try {
            Thread.sleep(timeToWaitInMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }*/


    @After
    public void tearDown() {

    }
}
